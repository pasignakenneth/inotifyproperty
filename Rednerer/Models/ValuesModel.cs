﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace Rednerer.Models
{
    public class ValuesModel : INotifyPropertyChanged
    {
        double _sliderValue;
        public double sliderValue
        {
            get
            {
                return _sliderValue;
            }
            set
            {
                _sliderValue = value;
                OnPropertyChanged(nameof(sliderValue));
                OnPropertyChanged(nameof(progressWidth));
            }
        }
        public double progressWidth
        {
            get
            {
                if (sliderValue.Equals(0.0))
                    return 0.0;
                return sliderValue / 10.0;
            }
        }


        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
