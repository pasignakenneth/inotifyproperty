﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Rednerer.Models;
using Xamarin.Forms;

namespace Rednerer
{
    public partial class MainPage : ContentPage
    {
        ValuesModel valuesModel = new ValuesModel();

        public MainPage()
        {
            InitializeComponent();
            BindingContext = valuesModel;
        }
        void Slider_ValueChanged(object sender, ValueChangedEventArgs e)
        {
            valuesModel.sliderValue = e.NewValue;
        }
    }
}
