﻿using System;
using Xamarin.Forms;

namespace Rednerer.Utilities.Renderers
{
    public class CustomProgressBar : ProgressBar
    {
        public static readonly BindableProperty BarHeightProperty = BindableProperty.Create(nameof(BarHeight), typeof(string), typeof(CustomProgressBar), "1");

        public string BarHeight
        {
            get
            {
                return (string)GetValue(BarHeightProperty);
            }
            set
            {
                SetValue(BarHeightProperty, value);
            }
        }
    }
}
