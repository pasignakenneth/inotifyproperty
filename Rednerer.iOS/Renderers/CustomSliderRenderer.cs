﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;
using Rednerer.iOS.Renderers;
using Rednerer.Utilities.Renderers;

[assembly: ExportRenderer(typeof(CustomSlider), typeof(CustomSliderRenderer))]
namespace Rednerer.iOS.Renderers
{
    public class CustomSliderRenderer : SliderRenderer
    {
        
    }
}
