﻿using System;
using CoreGraphics;
using Rednerer.iOS.Renderers;
using Rednerer.Utilities.Renderers;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(CustomProgressBar), typeof(CustomProgressBarRenderer))]
namespace Rednerer.iOS.Renderers
{
    public class CustomProgressBarRenderer : ProgressBarRenderer
    {
        CustomProgressBar custom { get; set; }

        protected override void OnElementChanged(ElementChangedEventArgs<ProgressBar> e)
        {
            base.OnElementChanged(e);
            if(e.NewElement != null || Control!=null)
            {
                this.custom = (CustomProgressBar)this.Element;
            }

        }

        public override void LayoutSubviews()
        {
            base.LayoutSubviews();


            var x = 1.0f;
            var y = custom.BarHeight;

            CGAffineTransform transform = CGAffineTransform.MakeScale(x, float.Parse(y));
            this.Transform = transform;
        }
    }
}
